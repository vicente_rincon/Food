//
//  MealApp.swift
//  Shared
//
//  Created by Vicente Rincon on 03/01/22.
//

import SwiftUI

@main
struct MealApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
