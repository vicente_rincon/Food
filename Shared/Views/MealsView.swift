//
//  MealsView.swift
//  Meal
//
//  Created by Vicente Rincon on 04/01/22.
//

import Foundation
import SwiftUI

struct MealsView: View {
    var categoryNameFinal: String = "beef"
    var body: some View {
        ZStack{
            Color("bgSecondaryColor").ignoresSafeArea()
            VStack{
                /*Image("appLogo")
                    .resizable().aspectRatio(contentMode: .fit).frame(width: 250).padding(.vertical, 8.0)*/
                SubModelMeal(categoryName: categoryNameFinal)
            }
        }.navigationBarTitle("", displayMode: .inline)
            .accentColor(Color("txMain"))
    }
}
struct SubModelMeal:View{
    var categoryName: String = ""
    @State var searchText:String = ""
    @State private var allMeals : [Meal] = []
    @State private var filterCategories : [Meal] = []
    var loadCategory = false
    var body: some View{
        HStack{
            Button(action: {
                
            }, label: {
                Image(systemName: "magnifyingglass").foregroundColor(Color("txSecondary") )
                            
                }
            )
            ZStack(alignment: .leading) {
                if searchText.isEmpty {
                    Text("Search Meal").foregroundColor(Color("txSecondary"))
                }
                TextField("", text: $searchText)
                    .foregroundColor(Color("txSecondary"))
                    .onChange(of: searchText) { newValue in
                        filterCategories = []
                        allMeals.forEach { meal in
                            if meal.strMeal.lowercased().contains(searchText.lowercased()){
                                filterCategories.append(meal)
                            }
                        }
                        print(filterCategories)
                    }
            }
        }   .padding([.top, .leading, .bottom], 8.0)
            .background(Color("bgColor"))
            .clipShape(Capsule())
        ScrollView(showsIndicators: false){
            if !searchText.isEmpty {
                Text("Search Results").font(.title3).foregroundColor(Color("txSecondary")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                    .padding(.all, 8.0)
                    .textCase(.uppercase)
                if filterCategories.count > 0 {
                    ForEach(filterCategories, id: \.self){meal in
                        MealItem(meal: meal)
                    }
                } else {
                    Text("No results").font(.title3).foregroundColor(Color("txMain")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .center)
                        .padding(.all, 8)
                        .textCase(.uppercase)
                }
            } else {
                VStack{
                    Text(categoryName).font(.title3).foregroundColor(Color("txMain")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .center)
                        .padding(.top)
                        .textCase(.uppercase)
                    ForEach(allMeals, id: \.self) {
                        meal in
                        MealItem(meal: meal)
                    }
                    //CategorieItem()
                }
            }
        }
        .onAppear {
            MealsModelService().loadMeals(categoryName: categoryName) { meals in
                allMeals = meals
            }
        }
    }
    func busqueda(){
        print("Search: \(searchText)")
    }
}
struct MealItem: View {
    var meal: Meal
    @State var mealviewIsActive:Bool = false
    @State var infoMeal = DetailMeal()
    @State var isLoading: Bool = false
    var body: some View{
        ZStack{
            Color("bgSecondaryColor")
            if isLoading{
                ProgressView("Loading...")
                    .scaleEffect(2)
            }
            Button(
                action: {
                    MealsModelService().loadDetailMeal(idMeal: meal.idMeal) { detailMeal in
                        infoMeal = detailMeal
                        let iDYoutube = infoMeal.strYoutube.replace(target: "https://www.youtube.com/watch?v=", withString: "")
                        infoMeal.strYoutube = iDYoutube
                        isLoading = false
                        mealviewIsActive = true
                    }
                },
                label: {
                    VStack(spacing: 0){
                        AsyncImage(
                            url: URL(string: meal.strMealThumb)
                        )
                        .cornerRadius(10)
                        .frame(maxWidth: 200, maxHeight: 200)
                        .scaledToFill()
                        //.clipShape(Capsule())
                        Text(meal.strMeal).frame(minWidth: 0,  maxWidth: .infinity,  alignment: .center).foregroundColor(Color("txSecondary")).font(Font.system(size:25)).padding(.all, 8).background(Color("bgColor").opacity(0.9)).cornerRadius(10)
                    }
                        
                }
            ).padding(.all , 16.0)
        }.cornerRadius(1)
        NavigationLink(
            destination: DetailMealView(infoMeal: infoMeal),
            isActive: $mealviewIsActive,
            label: {
                EmptyView()
            }
        )
    }
}
