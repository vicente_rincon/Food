//
//  DetailMealView.swift
//  Meal
//
//  Created by Vicente Rincon on 04/01/22.
//

import Foundation
import SwiftUI
import AVKit
struct DetailMealView: View {
    var infoMeal : DetailMeal = DetailMeal()
    var body: some View {
        ZStack{
            Color("bgColor").ignoresSafeArea()
            DetailElementView(infoMeal: infoMeal)
        }.navigationBarTitle("", displayMode: .inline)
            .accentColor(Color("txMain"))
    }
}
struct DetailElementView: View{
    var idMeal: String = ""
    @State var infoMeal : DetailMeal = DetailMeal()
    @State var ingredients: [String] = []
    @State var measures: [String] = []
    var body: some View{
        VStack{
            VideoView(videoID: infoMeal.strYoutube)
                .frame(maxWidth: .infinity, minHeight:0, maxHeight: 370.0)
                .ignoresSafeArea()
                .padding(.bottom, 0.0)
                .background(Color("bgSecondaryColor"))
            ScrollView{
                VStack{
                    
                    Text("\(infoMeal.strMeal) - \(infoMeal.strCategory)").frame(minWidth: 0,  maxWidth: .infinity,  alignment: .leading).foregroundColor(Color("txSecondary")).font(Font.system(size:25)).padding(.all, 8)
                        .background(Color("bgColor"))
                    HStack{
                        Text("Area: \(infoMeal.strArea)").frame(minWidth: 0,  maxWidth: .infinity,  alignment: .leading).foregroundColor(Color("txMain")).font(Font.system(size:18)).padding(.horizontal, 8)
                            .background(Color("bgSecondaryColor"))
                    }
                }
                Text("Ingredients").frame(minWidth: 0,  maxWidth: .infinity,  alignment: .leading).foregroundColor(Color("txSecondary")).font(Font.system(size:25)).padding(.all, 8)
                VStack{
                    ForEach(0..<ingredients.count, id: \.self){
                        index in
                        HStack{
                            
                            Text(ingredients[index]).frame(minWidth: 0,  maxWidth: .infinity,  alignment: .leading).foregroundColor(Color(((index%2) != 0) ? "txSecondary" : "txMain")).font(Font.system(size:18)).padding(.horizontal, 8)
                            Text(measures[index]).frame(minWidth: 0,  maxWidth: .infinity,  alignment: .leading).foregroundColor(Color(((index%2) != 0) ? "txSecondary" : "txMain")).font(Font.system(size:18)).padding(.horizontal, 8)
                        }.background(Color(((index%2) != 0) ? "bgColor" : "bgSecondaryColor"))
                    }
                }
                Divider()
                Text("Instructions").frame(minWidth: 0,  maxWidth: .infinity,  alignment: .leading).foregroundColor(Color("txMain")).font(Font.system(size:25)).padding(.all, 8).background(Color("bgSecondaryColor"))
                Text(infoMeal.strInstructions).frame(minWidth: 0,  maxWidth: .infinity,  alignment: .center).foregroundColor(Color("txSecondary")).font(Font.system(size:18)).padding(.all, 8)
            }.padding(.top, 0.0)
            
        }.onAppear {
            ingredients = loadIngredient(infoMeal: infoMeal)
            measures = loadMeasure(infoMeal: infoMeal)
        }
    }
}


