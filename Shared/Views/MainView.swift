//
//  ContentView.swift
//  Shared
//
//  Created by Vicente Rincon on 03/01/22.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        NavigationView{
            ZStack{
                Color("bgColor").ignoresSafeArea()
                VStack{
                    /*Image("appLogo")
                        .resizable().aspectRatio(contentMode: .fit).frame(width: 250).padding(.vertical, 8.0)*/
                    SubModuloHome()
                }
            }.navigationBarHidden(true)
        }
    }
}
struct SubModuloHome:View{
    @State var searchText:String = ""
    @StateObject private var serviceCategory = CategoryModelService()
    @State private var allCategories : [Category] = []
    @State private var filterCategories : [Category] = []
    @State private var randomMeal : [Meal] = []
    @State var categoryviewIsActive:Bool = false
    var loadCategory = false
    var body: some View{
        HStack{
            Button(action: {
                
            }, label: {
                Image(systemName: "magnifyingglass").foregroundColor(Color("txMain") )
                            
                }
            )
            ZStack(alignment: .leading) {
                if searchText.isEmpty {
                    Text("Search Category").foregroundColor(Color("txMain"))
                }
                TextField("", text: $searchText)
                    .foregroundColor(Color("txMain"))
                    .onChange(of: searchText) { newValue in
                        filterCategories = []
                        allCategories.forEach { category in
                            if category.strCategory.lowercased().contains(searchText.lowercased()){
                                filterCategories.append(category)
                            }
                        }
                        print(filterCategories)
                    }
            }
        }   .padding([.top, .leading, .bottom], 8.0)
            .background(Color("bgSecondaryColor"))
            .clipShape(Capsule())
        ScrollView(showsIndicators: false){
            if !searchText.isEmpty {
                Text("Search Results").font(.title3).foregroundColor(Color("txSecondary")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                    .padding(.all, 8.0)
                    .textCase(.uppercase)
                if filterCategories.count > 0 {
                    ForEach(filterCategories, id: \.self){category in
                        CategoryItem(category: category)
                    }
                } else {
                    Text("No results").font(.title3).foregroundColor(Color("txSecondary")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .center)
                        .padding(.all, 8)
                        .textCase(.uppercase)
                }
            } else {
                VStack{
                    Text("New recommendation").font(.title3).foregroundColor(Color("txSecondary")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                        .padding(.all, 8)
                        .textCase(.uppercase)
                    ForEach(randomMeal, id: \.self){
                        meal in
                        MealItem(meal: meal)
                    }
                    Text("Categories").font(.title3).foregroundColor(Color("txSecondary")).bold().frame(minWidth: 0, maxWidth: .infinity,alignment: .leading)
                        .padding([.horizontal, .top], 8)
                        .textCase(.uppercase)
                    ForEach(allCategories, id: \.self) {
                        category in
                        CategoryItem(category: category)
                    }
                    //CategorieItem()
                }
            }
        }
        .onAppear {
            CategoryModelService().loadCategories { categories in
                allCategories = categories
                /*let randomInt = Int.random(in: 0..<(allCategories.count))
                randomCategories.append(allCategories[randomInt])*/
            }
            MealsModelService().loadRandom { meals in
                randomMeal = meals
            }
        }
    }
    func busqueda(){
        print("Search: \(searchText)")
    }
}
struct CategoryItem: View {
    var category: Category
    @State var categoryviewIsActive:Bool = false
    var body: some View{
        ZStack{
            Color("bgSecondaryColor")
            Button(
                action: {
                    categoryviewIsActive = true
                },
                label: {
                    VStack(spacing: 0){
                        AsyncImage(url: URL(string: category.strCategoryThumb))
                            .cornerRadius(10)
                        Text(category.strCategory).frame(minWidth: 0,  maxWidth: .infinity,  alignment: .center).foregroundColor(Color("txMain")).font(Font.system(size:25)).padding(.all, 8)
                        Text(category.strCategoryDescription).frame(minWidth: 0,  maxWidth: .infinity,  alignment: .center).foregroundColor(Color("txMain")).lineLimit(4)
                    }
                }
            ).padding(.all , 16.0)
        }.cornerRadius(10)
            .padding([.bottom, .horizontal], 10.0)
        NavigationLink(
            destination: MealsView(categoryNameFinal: category.strCategory),
            isActive: $categoryviewIsActive,
            label: {
                EmptyView()
            }
        )
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
