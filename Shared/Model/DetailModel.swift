//
//  DetailModel.swift
//  Meal
//
//  Created by Vicente Rincon on 04/01/22.
//

import Foundation
import SwiftUI
import WebKit

struct VideoView: UIViewRepresentable{
    let videoID: String
    func makeUIView(context: Context) -> WKWebView{
        return WKWebView()
    }
    func updateUIView(_ uiView: WKWebView, context: Context){
        guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)") else {return}
        uiView.scrollView.isScrollEnabled = false
        uiView.load(URLRequest(url: youtubeURL))
    }
}

struct DetailMeal: Codable, Hashable{
    var strMeal:String
    var strMealThumb:String
    var idMeal: String
    var strArea: String
    var strInstructions: String
    var strCategory: String
    var strYoutube: String
    var strMeasure1: String
    var strMeasure2: String
    var strMeasure3: String
    var strMeasure4: String
    var strMeasure5: String
    var strMeasure6: String
    var strMeasure7: String
    var strMeasure8: String
    var strMeasure9: String
    var strMeasure10: String
    var strMeasure11: String
    var strMeasure12: String
    var strMeasure13: String
    var strMeasure14: String
    var strMeasure15: String
    var strMeasure16: String
    var strMeasure17: String
    var strMeasure18: String
    var strMeasure19: String
    var strMeasure20: String
    var strIngredient1: String
    var strIngredient2: String
    var strIngredient3: String
    var strIngredient4: String
    var strIngredient5: String
    var strIngredient6: String
    var strIngredient7: String
    var strIngredient8: String
    var strIngredient9: String
    var strIngredient10: String
    var strIngredient11: String
    var strIngredient12: String
    var strIngredient13: String
    var strIngredient14: String
    var strIngredient15: String
    var strIngredient16: String
    var strIngredient17: String
    var strIngredient18: String
    var strIngredient19: String
    var strIngredient20: String
    init(strMeal: String = "",
         strMealThumb: String = "",
         idMeal: String = "",
         strArea: String = "",
         strInstructions: String = "",
         strCategory: String = "",
         strYoutube: String = "",
         strMeasure1: String = "",
         strMeasure2: String = "",
         strMeasure3: String = "",
         strMeasure4: String = "",
         strMeasure5: String = "",
         strMeasure6: String = "",
         strMeasure7: String = "",
         strMeasure8: String = "",
         strMeasure9: String = "",
         strMeasure10: String = "",
         strMeasure11: String = "",
         strMeasure12: String = "",
         strMeasure13: String = "",
         strMeasure14: String = "",
         strMeasure15: String = "",
         strMeasure16: String = "",
         strMeasure17: String = "",
         strMeasure18: String = "",
         strMeasure19: String = "",
         strMeasure20: String = "",
         strIngredient1: String = "",
         strIngredient2: String = "",
         strIngredient3: String = "",
         strIngredient4: String = "",
         strIngredient5: String = "",
         strIngredient6: String = "",
         strIngredient7: String = "",
         strIngredient8: String = "",
         strIngredient9: String = "",
         strIngredient10: String = "",
         strIngredient11: String = "",
         strIngredient12: String = "",
         strIngredient13: String = "",
         strIngredient14: String = "",
         strIngredient15: String = "",
         strIngredient16: String = "",
         strIngredient17: String = "",
         strIngredient18: String = "",
         strIngredient19: String = "",
         strIngredient20: String = "")
    {
        self.strMeal = strMeal
        self.strMealThumb = strMealThumb
        self.idMeal = idMeal
        self.strArea = strArea
        self.strInstructions = strInstructions
        self.strCategory = strCategory
        self.strYoutube = strYoutube
        self.strMeasure1 = strMeasure1
        self.strMeasure2 = strMeasure2
        self.strMeasure3 = strMeasure3
        self.strMeasure4 = strMeasure4
        self.strMeasure5 = strMeasure5
        self.strMeasure6 = strMeasure6
        self.strMeasure7 = strMeasure7
        self.strMeasure8 = strMeasure8
        self.strMeasure9 = strMeasure9
        self.strMeasure10 = strMeasure10
        self.strMeasure11 = strMeasure11
        self.strMeasure12 = strMeasure12
        self.strMeasure13 = strMeasure13
        self.strMeasure14 = strMeasure14
        self.strMeasure15 = strMeasure15
        self.strMeasure16 = strMeasure16
        self.strMeasure17 = strMeasure17
        self.strMeasure18 = strMeasure18
        self.strMeasure19 = strMeasure19
        self.strMeasure20 = strMeasure20
        self.strIngredient1 = strIngredient1
        self.strIngredient2 = strIngredient2
        self.strIngredient3 = strIngredient3
        self.strIngredient4 = strIngredient4
        self.strIngredient5 = strIngredient5
        self.strIngredient6 = strIngredient6
        self.strIngredient7 = strIngredient7
        self.strIngredient8 = strIngredient8
        self.strIngredient9 = strIngredient9
        self.strIngredient10 = strIngredient10
        self.strIngredient11 = strIngredient11
        self.strIngredient12 = strIngredient12
        self.strIngredient13 = strIngredient13
        self.strIngredient14 = strIngredient14
        self.strIngredient15 = strIngredient15
        self.strIngredient16 = strIngredient16
        self.strIngredient17 = strIngredient17
        self.strIngredient18 = strIngredient18
        self.strIngredient19 = strIngredient19
        self.strIngredient20 = strIngredient20
    }
    enum CodingKeys: String, CodingKey {
            case strMeal = "strMeal"
            case strMealThumb = "strMealThumb"
            case idMeal = "idMeal"
            case strArea = "strArea"
            case strInstructions = "strInstructions"
            case strCategory = "strCategory"
            case strYoutube = "strYoutube"
            case strIngredient1 = "strIngredient1"
            case strIngredient2 = "strIngredient2"
            case strIngredient3 = "strIngredient3"
            case strIngredient4 = "strIngredient4"
            case strIngredient5 = "strIngredient5"
            case strIngredient6 = "strIngredient6"
            case strIngredient7 = "strIngredient7"
            case strIngredient8 = "strIngredient8"
            case strIngredient9 = "strIngredient9"
            case strIngredient10 = "strIngredient10"
            case strIngredient11 = "strIngredient11"
            case strIngredient12 = "strIngredient12"
            case strIngredient13 = "strIngredient13"
            case strIngredient14 = "strIngredient14"
            case strIngredient15 = "strIngredient15"
            case strIngredient16 = "strIngredient16"
            case strIngredient17 = "strIngredient17"
            case strIngredient18 = "strIngredient18"
            case strIngredient19 = "strIngredient19"
            case strIngredient20 = "strIngredient20"
            case strMeasure1 = "strMeasure1"
            case strMeasure2 = "strMeasure2"
            case strMeasure3 = "strMeasure3"
            case strMeasure4 = "strMeasure4"
            case strMeasure5 = "strMeasure5"
            case strMeasure6 = "strMeasure6"
            case strMeasure7 = "strMeasure7"
            case strMeasure8 = "strMeasure8"
            case strMeasure9 = "strMeasure9"
            case strMeasure10 = "strMeasure10"
            case strMeasure11 = "strMeasure11"
            case strMeasure12 = "strMeasure12"
            case strMeasure13 = "strMeasure13"
            case strMeasure14 = "strMeasure14"
            case strMeasure15 = "strMeasure15"
            case strMeasure16 = "strMeasure16"
            case strMeasure17 = "strMeasure17"
            case strMeasure18 = "strMeasure18"
            case strMeasure19 = "strMeasure19"
            case strMeasure20 = "strMeasure20"
    }
    init(from decoder: Decoder) throws {

            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.strMeal = try container.decodeIfPresent(String.self, forKey: .strMeal) ?? ""
            self.strMealThumb = try container.decodeIfPresent(String.self, forKey: .strMealThumb) ?? ""
            self.idMeal = try container.decodeIfPresent(String.self, forKey: .idMeal) ?? ""
            self.strArea = try container.decodeIfPresent(String.self, forKey: .strArea) ?? ""
            self.strInstructions = try container.decodeIfPresent(String.self, forKey: .strInstructions) ?? ""
            self.strCategory = try container.decodeIfPresent(String.self, forKey: .strCategory) ?? ""
            self.strYoutube = try container.decodeIfPresent(String.self, forKey: .strYoutube) ?? ""
            self.strIngredient1 = try container.decodeIfPresent(String.self, forKey: .strIngredient1) ?? ""
            self.strIngredient2 = try container.decodeIfPresent(String.self, forKey: .strIngredient2) ?? ""
            self.strIngredient3 = try container.decodeIfPresent(String.self, forKey: .strIngredient3) ?? ""
            self.strIngredient4 = try container.decodeIfPresent(String.self, forKey: .strIngredient4) ?? ""
            self.strIngredient5 = try container.decodeIfPresent(String.self, forKey: .strIngredient5) ?? ""
            self.strIngredient6 = try container.decodeIfPresent(String.self, forKey: .strIngredient6) ?? ""
            self.strIngredient7 = try container.decodeIfPresent(String.self, forKey: .strIngredient7) ?? ""
            self.strIngredient8 = try container.decodeIfPresent(String.self, forKey: .strIngredient8) ?? ""
            self.strIngredient9 = try container.decodeIfPresent(String.self, forKey: .strIngredient9) ?? ""
            self.strIngredient10 = try container.decodeIfPresent(String.self, forKey: .strIngredient10) ?? ""
            self.strIngredient11 = try container.decodeIfPresent(String.self, forKey: .strIngredient11) ?? ""
            self.strIngredient12 = try container.decodeIfPresent(String.self, forKey: .strIngredient12) ?? ""
            self.strIngredient13 = try container.decodeIfPresent(String.self, forKey: .strIngredient13) ?? ""
            self.strIngredient14 = try container.decodeIfPresent(String.self, forKey: .strIngredient14) ?? ""
            self.strIngredient15 = try container.decodeIfPresent(String.self, forKey: .strIngredient15) ?? ""
            self.strIngredient16 = try container.decodeIfPresent(String.self, forKey: .strIngredient16) ?? ""
            self.strIngredient17 = try container.decodeIfPresent(String.self, forKey: .strIngredient17) ?? ""
            self.strIngredient18 = try container.decodeIfPresent(String.self, forKey: .strIngredient18) ?? ""
            self.strIngredient19 = try container.decodeIfPresent(String.self, forKey: .strIngredient19) ?? ""
            self.strIngredient20 = try container.decodeIfPresent(String.self, forKey: .strIngredient20) ?? ""
            self.strMeasure1 = try container.decodeIfPresent(String.self, forKey: .strMeasure1) ?? ""
            self.strMeasure2 = try container.decodeIfPresent(String.self, forKey: .strMeasure2) ?? ""
            self.strMeasure3 = try container.decodeIfPresent(String.self, forKey: .strMeasure3) ?? ""
            self.strMeasure4 = try container.decodeIfPresent(String.self, forKey: .strMeasure4) ?? ""
            self.strMeasure5 = try container.decodeIfPresent(String.self, forKey: .strMeasure5) ?? ""
            self.strMeasure6 = try container.decodeIfPresent(String.self, forKey: .strMeasure6) ?? ""
            self.strMeasure7 = try container.decodeIfPresent(String.self, forKey: .strMeasure7) ?? ""
            self.strMeasure8 = try container.decodeIfPresent(String.self, forKey: .strMeasure8) ?? ""
            self.strMeasure9 = try container.decodeIfPresent(String.self, forKey: .strMeasure9) ?? ""
            self.strMeasure10 = try container.decodeIfPresent(String.self, forKey: .strMeasure10) ?? ""
            self.strMeasure11 = try container.decodeIfPresent(String.self, forKey: .strMeasure11) ?? ""
            self.strMeasure12 = try container.decodeIfPresent(String.self, forKey: .strMeasure12) ?? ""
            self.strMeasure13 = try container.decodeIfPresent(String.self, forKey: .strMeasure13) ?? ""
            self.strMeasure14 = try container.decodeIfPresent(String.self, forKey: .strMeasure14) ?? ""
            self.strMeasure15 = try container.decodeIfPresent(String.self, forKey: .strMeasure15) ?? ""
            self.strMeasure16 = try container.decodeIfPresent(String.self, forKey: .strMeasure16) ?? ""
            self.strMeasure17 = try container.decodeIfPresent(String.self, forKey: .strMeasure17) ?? ""
            self.strMeasure18 = try container.decodeIfPresent(String.self, forKey: .strMeasure18) ?? ""
            self.strMeasure19 = try container.decodeIfPresent(String.self, forKey: .strMeasure19) ?? ""
            self.strMeasure20 = try container.decodeIfPresent(String.self, forKey: .strMeasure20) ?? ""

        }
}
extension String {
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target,
                                         with: withString,
                                         options: NSString.CompareOptions.literal,
                                         range: nil)
    }
}
func loadIngredient(infoMeal: DetailMeal) -> ([String]){
    let ingredients = [
        infoMeal.strIngredient1,
        infoMeal.strIngredient2,
        infoMeal.strIngredient3,
        infoMeal.strIngredient4,
        infoMeal.strIngredient5,
        infoMeal.strIngredient6,
        infoMeal.strIngredient7,
        infoMeal.strIngredient8,
        infoMeal.strIngredient9,
        infoMeal.strIngredient10,
        infoMeal.strIngredient11,
        infoMeal.strIngredient12,
        infoMeal.strIngredient13,
        infoMeal.strIngredient14,
        infoMeal.strIngredient15,
        infoMeal.strIngredient16,
        infoMeal.strIngredient17,
        infoMeal.strIngredient18,
        infoMeal.strIngredient19,
        infoMeal.strIngredient20
    ]
    var finalIngredient: [String] = []
    for ingredient in ingredients {
        if !ingredient.isEmpty && ingredient != " "{
            finalIngredient.append(ingredient)
        } else {
            break
        }
    }
    return finalIngredient
}
func loadMeasure(infoMeal: DetailMeal) -> ([String]){
    return [
            infoMeal.strMeasure1,
            infoMeal.strMeasure2,
            infoMeal.strMeasure3,
            infoMeal.strMeasure4,
            infoMeal.strMeasure5,
            infoMeal.strMeasure6,
            infoMeal.strMeasure7,
            infoMeal.strMeasure8,
            infoMeal.strMeasure9,
            infoMeal.strMeasure10,
            infoMeal.strMeasure11,
            infoMeal.strMeasure12,
            infoMeal.strMeasure13,
            infoMeal.strMeasure14,
            infoMeal.strMeasure15,
            infoMeal.strMeasure16,
            infoMeal.strMeasure17,
            infoMeal.strMeasure18,
            infoMeal.strMeasure19,
            infoMeal.strMeasure20
            
    ]
}
