//
//  CategoriesModel.swift
//  Meal
//
//  Created by Vicente Rincon on 03/01/22.
//

import Foundation
struct Categories:Codable{
    var categories:[Category]
}
struct Results:Codable{
    var results:[Category]
}
struct Category: Codable, Hashable{
    var strCategory:String
    var strCategoryThumb:String
    var strCategoryDescription:String
    var idCategory: String
}

