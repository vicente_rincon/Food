//
//  MealsModel.swift
//  Meal
//
//  Created by Vicente Rincon on 04/01/22.
//

import Foundation
struct Meals:Codable{
    var meals:[Meal]
}
struct ResultsMeals:Codable{
    var results:[Meal]
}
struct Meal: Codable, Hashable{
    var strMeal:String
    var strMealThumb:String
    var idMeal: String
}
