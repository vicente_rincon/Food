//
//  MealsModelView.swift
//  Meal
//
//  Created by Vicente Rincon on 04/01/22.
//

import Foundation
class MealsModelService: ObservableObject {
    @Published var mealsInfo = [Meal]()
    @Published var mealDetails = [DetailMeal]()
    let urlServer = "https://www.themealdb.com/api/json/v1/1/filter.php?c="
    let urlRandomMeal = "https://www.themealdb.com/api/json/v1/1/random.php"
    let urlGetDetailMeal = "https://www.themealdb.com/api/json/v1/1/lookup.php?i="
    init() {
    }
    public func loadMeals(categoryName: String, completion: @escaping([Meal]) -> ()) {
        let url = URL(string: "\(urlServer)\(categoryName)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            do {
                if let jsonData = data {
                    print(jsonData)
                    let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                    if let json = responseJSON as? [String: Any] {
                        do {
                            let jsonDataCategories = try JSONSerialization.data(withJSONObject: json["meals"] ?? [:], options: .prettyPrinted)
                            let decodedData = try JSONDecoder().decode([Meal].self, from: jsonDataCategories)
                            DispatchQueue.main.async {
                                self.mealsInfo.append(contentsOf: decodedData)
                                completion(self.mealsInfo)
                            }
                        }
                    } else {
                        print("Error")
                    }
                } else {
                    print("No existen datos en el json recuperado")
                }
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
    public func loadRandom(completion: @escaping([Meal]) -> ()){
        let url = URL(string: urlRandomMeal)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            do {
                if let jsonData = data {
                    print(jsonData)
                    let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                    if let json = responseJSON as? [String: Any] {
                        do {
                            let jsonDataCategories = try JSONSerialization.data(withJSONObject: json["meals"] ?? [:], options: .prettyPrinted)
                            let decodedData = try JSONDecoder().decode([Meal].self, from: jsonDataCategories)
                            DispatchQueue.main.async {
                                self.mealsInfo.append(contentsOf: decodedData)
                                completion(self.mealsInfo)
                            }
                        }
                    } else {
                        print("Error")
                    }
                } else {
                    print("No existen datos en el json recuperado")
                }
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
    public func loadDetailMeal(idMeal: String, completion: @escaping(DetailMeal) ->()){
        let url = URL(string: "\(urlGetDetailMeal)\(idMeal)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            do {
                if let jsonData = data {
                    print(jsonData)
                    let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                    if let json = responseJSON as? [String: Any] {
                        do {
                            let jsonDataCategories = try JSONSerialization.data(withJSONObject: json["meals"] ?? [:], options: .prettyPrinted)
                            let decodedData = try JSONDecoder().decode([DetailMeal].self, from: jsonDataCategories)
                            DispatchQueue.main.async {
                                self.mealDetails = decodedData
                                completion(self.mealDetails[0])
                            }
                        }
                    } else {
                        print("Error")
                    }
                } else {
                    print("No existen datos en el json recuperado")
                }
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
}
