//
//  CategoriesModelView.swift
//  Meal
//
//  Created by Vicente Rincon on 03/01/22.
//

import Foundation
class CategoryModelService: ObservableObject {
    @Published var categoriesInfo = [Category]()
    var urlServer = "https://www.themealdb.com/api/json/v1/1/categories.php"
    init() {
    }
    public func loadCategories(completion: @escaping([Category]) -> ()) {
        let url = URL(string: urlServer)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            do {
                if let jsonData = data {
                    print("tamañoJSON: \(jsonData)")
                    print(jsonData)
                    let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                    if let json = responseJSON as? [String: Any] {
                        do {
                            let jsonDataCategories = try JSONSerialization.data(withJSONObject: json["categories"] ?? [:], options: .prettyPrinted)
                            let decodedData = try JSONDecoder().decode([Category].self, from: jsonDataCategories)
                            DispatchQueue.main.async {
                                self.categoriesInfo.append(contentsOf: decodedData)
                                completion(self.categoriesInfo)
                            }
                        }
                    } else {
                        print("Error")
                    }
                } else {
                    print("No existen datos en el json recuperado")
                }
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
}
